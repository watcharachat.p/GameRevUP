package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MapRenderer 
{
	private Map map;
    private SpriteBatch batch;
    private Hero hero;
    
    private Texture floorImage = new Texture("Tile1_Body.png");
	private Texture floorTopImage = new Texture("Tile1_Top.png");
	private Texture ceilingImage = new Texture("Tile2_Body.png");
	private Texture ceilingTopImage = new Texture("Tile2_Top.png");
	private Texture backDropImage = new Texture("Tile3_Body.png");
	
	private int counter = 0;
 
    public MapRenderer(SpriteBatch batch,Game game) 
    {
        this.map = game.getMap();
        this.batch = batch;
        this.hero = game.getHero();
    }
 
    public void render() 
    {
       batch.begin();
       for(int r = 0 ; r < map.getHeight() ; r++) 
       {
            for(int c = 0 ; c < map.getWidth() ; c++) 
            {
            	 int x = c * GameRenderer.BLOCK_WIDTH;
                 int y = RevUP.HEIGHT - 
                         (r * GameRenderer.BLOCK_HEIGHT) - GameRenderer.BLOCK_HEIGHT;
                 drawBackDrop(map, r, c, x-(counter%90), y);
                 drawFloor(map, r, c, x-(counter%90), y);
            }
       }
       if(hero.starter)
       {
    	   counter += 3;
       }
       batch.end();
    }
    
    private void drawBackDrop(Map map,int r,int c,int x,int y)
    {
    	if(map.getBackDrop(r,c) == '.') 
        {
            batch.draw(backDropImage, x, y);
        } 
    }
    private void drawFloor(Map map,int r,int c,int x,int y)
    {
    	if (map.getFloorPlan(r,c) == '#') 
        {
            batch.draw(floorImage, x, y);
        } 
        else if (map.getFloorPlan(r,c) == '*') 
        {
            batch.draw(floorTopImage, x, y);
        }
        else if (map.getFloorPlan(r,c) == 'X')
        {
            batch.draw(ceilingImage, x, y);
        }
        else if (map.getFloorPlan(r,c) == 'Y')
        {
            batch.draw(ceilingTopImage, x, y);
        }
    }
}
