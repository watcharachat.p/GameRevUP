package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;

public class Map 
{   
	private int height;
	private int width;
	
	private String[] FloorPlan = new String [] 
	{
            "XXXXXXXXXXX",
            "YYYYYYYYYYY",
            "...........",
            "...........",
            "...........",
            "***********",
            "###########"
    };
	
	private String[] BackDrop = new String [] 
	{
			"...........",
			"...........",
			"...........",
			"...........",
			"...........",
			"...........",
			"..........."
	};
	
    public Map() 
    {
    	height = BackDrop.length;
        width = BackDrop[0].length();
    }
    
    public char getBackDrop(int r,int c)
    {
    	return BackDrop[r].charAt(c);
    }
    public char getFloorPlan(int r,int c)
    {
    	return FloorPlan[r].charAt(c);
    }
    
    public int getHeight()
    {
    	return height;
    }
    public int getWidth()
    {
    	return width;
    }
}
