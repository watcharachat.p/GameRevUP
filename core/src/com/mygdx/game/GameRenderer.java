package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class GameRenderer 
{
	private RevUP revUP;
	private SpriteBatch batch;
	private Game game;
	private Hero hero;
	//private Texture HeroImg;
	private Sound bgmSound = Gdx.audio.newSound(Gdx.files.internal("BGMSound.mp3"));
	private Texture ControlsImg;
	private MapRenderer mapRenderer;
	private WallRenderer wallRenderer;
	
	public static final int BLOCK_WIDTH = 90;
    public static final int BLOCK_HEIGHT = 60;
    
    private BitmapFont font;

	public GameRenderer(RevUP revUP,Game game)
	{
		this.revUP = revUP;
        batch = revUP.batch;
        this.game = game;
        this.hero = game.getHero();
        ControlsImg = new Texture("ControlsTip.png");
        mapRenderer = new MapRenderer(revUP.batch,game);
        wallRenderer = new WallRenderer(revUP.batch, game);
        font = new BitmapFont();
        this.bgmSound.loop(0.3f);
	}
	
	public void render(float delta) 
	{
		if(game.getLife() > 0)
		{
			mapRenderer.render();
			wallRenderer.render();
		}
		
		SpriteBatch batch = revUP.batch;
	    Vector2 pos = game.getHero().getPosition();
	    
	    batch.begin();
	    batch.draw(game.getHero().getSprite(), pos.x,pos.y);
	    
	    if(game.getLife() > 0)
	    {
	    	game.getHero().update();
	    }
	    drawUI();
	    if(!hero.starter)
	    {
	    	batch.draw(ControlsImg, 300, 130);
	    }
	    if (game.getLife() <= 0)
	    {
	    	this.bgmSound.stop();
	    	font.draw(batch, "GAME OVER", 400, 250);
	    	font.draw(batch, "Your Score", 405, 200);
	    	font.draw(batch, ""+game.getScore(), 405, 180);
	    	if(Gdx.input.isKeyPressed(Keys.ANY_KEY))
	    	{
	    		restartGame();
	    	}
	    }
	    batch.end();
	} 
	public WallRenderer getWallRenderer() 
	{
		return this.wallRenderer;
	}
	public Game getGame() 
	{
		return this.game;
	}
	
	private void drawUI()
	{
		font.draw(batch, "LIVES : " + game.getLife(), 60, 400);
	    font.draw(batch, "HI-SCORE : " + revUP.getHighScore(), 60, 37);
	    font.draw(batch, "SCORE : " + game.getScore(), 690, 37);
	}
	
	private void restartGame()
	{
		wallRenderer.removeAllWalls();
    	if (game.getScore() > revUP.getHighScore())
    	{
    		 revUP.setHighScore(game.getScore());
    	}
    	wallRenderer.removeAllWalls();
    	revUP.dispose();
    	revUP.create();
	}
}
