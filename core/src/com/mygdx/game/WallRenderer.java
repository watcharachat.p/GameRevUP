package com.mygdx.game;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class WallRenderer 
{
	//private Map map;
    private SpriteBatch batch;
    private  Wall tempWall;
    public int wallSpeed = 10;
    private int delay = 40;
    private int counter = 0;
    private Hero hero;
    
    private int speedControl = 5;
    private int speedMarker = 0;
    
    private Texture WallImage1 = new Texture("Wall0_Body.png");
    private Texture WallImage2 = new Texture("Wall1_Body.png");
    private Texture WallImage3 = new Texture("Wall2_Body.png");
	private Game game;
    
    public static ArrayList<Wall> walls1 = new ArrayList <Wall>();
    public static ArrayList<Wall> walls2 = new ArrayList <Wall>();
    public static ArrayList<Wall> walls3 = new ArrayList <Wall>();
    public static ArrayList<Wall> walls4 = new ArrayList <Wall>();
    public static ArrayList<Wall> walls5 = new ArrayList <Wall>();
    
    public static final int WALL_HEIGHT = 60;
    public static final int WALL_WIDTH = 42;
    
 
    public WallRenderer(SpriteBatch batch,Game game) 
    {
    	this.game = game;
    	this.hero = game.getHero();
        this.batch = batch;
    }
 
    public void render() 
    {
       batch.begin();
       wallUpdater(walls1,WallImage1);
       wallUpdater(walls2,WallImage1);
       wallUpdater(walls3,WallImage2);
       wallUpdater(walls4,WallImage2);
       wallUpdater(walls5,WallImage3);
     
       if (hero.starter)
       {
    	   counter = wallSpawner(counter, delay);
       }
       /*/tester//
       if(Gdx.input.isKeyJustPressed(Keys.Q))
       {
    	   generateWall(1);
       }
       if(Gdx.input.isKeyJustPressed(Keys.W))
       {
    	   generateWall(2);
       }
       if(Gdx.input.isKeyJustPressed(Keys.E))
       {
    	   generateWall(3);
       }
       if(Gdx.input.isKeyJustPressed(Keys.R))
       {
    	   generateWall(4);
       }
       if(Gdx.input.isKeyJustPressed(Keys.T))
       {
    	   generateWall(5);
       }
       //tester/*/
       batch.end();
    }
    
    private int wallSpawner(int counter,int delay)
    {
    	if(counter >= delay)
    	{
    		generateWall( (int )(Math.random() * 5 + 1) );
    		return 0;
    	}
    	return counter += 1;
    }

    public void removeWall(Wall w,ArrayList<Wall> walls)
    {
    		walls.remove(w);
    }
    private void generateWall(int type)
    {
    	if (type == 1)
        {
    		Wall w = new Wall(900, 72);
    		walls1.add(w);
        }
        if (type == 2)
        {
        	Wall w = new Wall(900, 420-72-60);
        	walls2.add(w);
        }
        if (type == 3)
        {
        	Wall w = new Wall(900, 72);
        	walls3.add(w);
        }
        if (type == 4)
        {
        	Wall w = new Wall(900, 420-72-120);
        	walls4.add(w);
        }
        if (type == 5)
        {
        	Wall w = new Wall(900, 72+60-12);
        	walls5.add(w);
        }
    }
    private void speedAdder()
    {
    	if(speedMarker >= speedControl)
    	{
    		speedMarker = 0;
    		wallSpeed += 1;
    		if(wallSpeed > 10)
    		{
    			wallSpeed = 10;
    		}
    		delay -= 5;
    		if(delay < 15)
    		{
    			delay = 15;
    		}
    	}
    	else
    	{
    		speedMarker += 1;
    	}
    }
    
    public boolean ifHitHero(Hero hero,ArrayList<Wall> walls,Wall wall)
    {
    	if (walls == walls1 || walls == walls2)
    	{
    		if (hero.getBound().overlaps
    				(new Rectangle((int)wall.getWallPosition().x, (int)wall.getWallPosition().y, 42, 60)) )
	        {
	            return true;
	        }
    	}
    	if (walls == walls3 || walls == walls4)
    	{
    		if (hero.getBound().overlaps
    				(new Rectangle((int)wall.getWallPosition().x, (int)wall.getWallPosition().y, 42, 120)) )
	        {
	            return true;
	        }
    	}
    	if (walls == walls5)
    	{
    		if (hero.getBound().overlaps
    				(new Rectangle((int)wall.getWallPosition().x, (int)wall.getWallPosition().y, 42, 180)) )
	        {
	            return true;
	        }
    	}
    	return false;
    }
    
    private void wallUpdater(ArrayList<Wall> walls,Texture WallImage)
    {		
    	for(int i = 0 ; i < walls.size() ; i++)
        {
     	   tempWall = walls.get(i);
     	   batch.draw(WallImage, tempWall.getWallPosition().x, tempWall.getWallPosition().y);
     	   tempWall.wallSet(tempWall.getWallPosition().x-wallSpeed,tempWall.getWallPosition().y);
     	   if (tempWall.getWallPosition().x <= -50)
     	   {
     		   removeWall(tempWall, walls);
     		   game.increaseScore();
     		   speedAdder();
     	   }
     	   if (ifHitHero(hero, walls, tempWall))
     	   {
     		   hero.damageTaken();
     		   removeWall(tempWall, walls);
     		   game.decreaseLife();
     	   }
        }
    }
    
    public void removeAllWalls()
    {
    	for(int i = 0 ; i < walls1.size() ; i++)
        {
     	   tempWall = walls1.get(i);
     	   removeWall(tempWall, walls1);
        }
    	for(int i = 0 ; i < walls2.size() ; i++)
        {
     	   tempWall = walls2.get(i);
     	   removeWall(tempWall, walls2);
        }
    	for(int i = 0 ; i < walls3.size() ; i++)
        {
     	   tempWall = walls3.get(i);
     	   removeWall(tempWall, walls3);
        }
    	for(int i = 0 ; i < walls4.size() ; i++)
        {
     	   tempWall = walls4.get(i);
     	   removeWall(tempWall, walls4);
        }
    	for(int i = 0 ; i < walls5.size() ; i++)
        {
     	   tempWall = walls5.get(i);
     	   removeWall(tempWall, walls5);
        }
    }
}
