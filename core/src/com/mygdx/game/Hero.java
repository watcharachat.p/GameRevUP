package com.mygdx.game;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

public class Hero 
{
	private Texture heroSprite = new Texture("Hero_stand00.png");
	private Sound jumpSound = Gdx.audio.newSound(Gdx.files.internal("JumpSound.wav"));
	private Sound swapSound = Gdx.audio.newSound(Gdx.files.internal("SwapSound.wav"));
	private Sound hitSound = Gdx.audio.newSound(Gdx.files.internal("HurtSound.wav"));
	private int activeFrame;
	private int activeState;
	private int frameRate = 2;
	private int frameCount = 0;
	public boolean starter = false;
	
	private int jumpStart = 0;
	private int fallAccel = 1;
	private int jumpSpeed;
	private int jumpPower = 20;
	private int maxJumpHeight = 90 + GameRenderer.BLOCK_HEIGHT;
	
	private boolean isFliped = false;
	
	public static final int STATE_STAND = 0;
	public static final int STATE_WALK = 1;
	public static final int STATE_HIT = 2;
	public static final int STATE_SLIDE = 3;
	public static final int STATE_JUMP = 4;
	
	private Vector2 position;	

	private Rectangle bounds;
	
	public Hero(int x,int y)
	{
		position = new Vector2(x+100,y);
		activeFrame = 0;
		activeState = 0;
	}
	
	public void update()
	{
		if (Gdx.input.isKeyJustPressed(Keys.SPACE))
		{
			isFliped = !isFliped;
			this.activeState = STATE_WALK;
			jumpStart = 0;
			this.swapSound.play(0.5f);
		}
		updater();
	}
	
	private void updater()
	{
		if (jumpStart == 0)
		{
			jumpSpeed = jumpPower;
			updateState();
		}
		else
		{
			jumpStart++;
			if(this.position.y < maxJumpHeight || this.position.y > 420 - 85 - maxJumpHeight)
			{
				if(!isFliped)
					this.position.y += (jumpSpeed);
				else
					this.position.y -= (jumpSpeed);
			}
				jumpSpeed -= fallAccel;
			if(jumpSpeed <= 0)
			{
				jumpStart = 0;
			}
		}
		updatePosition();
		frameCount += 1;
		if (frameCount == this.frameRate)
		{
			frameCount = 0;
			heroSprite = updateSprite();
		}
	}
	private void updatePosition()
	{
		if (activeState == STATE_WALK)
		{
			if (!isFliped)
			{
				if (this.position.y > 60)
				{
					this.position.y -= jumpSpeed;
					jumpSpeed += fallAccel;
					if (this.position.y < 60)
					{
						this.position.y = 60;
					}
				}
			}
			else
			{
				if (this.position.y < 420-85-60)
				{
					this.position.y += jumpSpeed;
					jumpSpeed += fallAccel;
					if (this.position.y > 420-85-60)
					{
						this.position.y = 420-85-60;
					}
				}
			}
		}
	}
	private Texture updateSprite()
	{
		String adder = "";
		if (isFliped)
		{
			adder = "f";
		}
		if (this.stateCheck() == STATE_STAND)
		{
			this.activeFrame += 1;
			if (this.frameCheck() > 2)
			{
				this.activeFrame = 0;
			}
			return new Texture(adder + "Hero_stand0" + this.activeFrame+".png");
		}
		if (this.stateCheck() == STATE_WALK)
		{
			this.activeFrame += 1;
			if(this.frameCheck() > 3)
			{
				this.activeFrame = 0;
			}
			return new Texture(adder + "Hero_walk0"+this.activeFrame+".png");
		}
		if (this.stateCheck() == STATE_HIT)
		{
			this.activeFrame += 1;
			if(this.frameCheck() > 2)
			{
				this.activeFrame = 0;
			}
			return new Texture(adder + "Hero_hit0"+this.activeFrame+".png");
		}
		if (this.stateCheck() == STATE_SLIDE)
		{
			return new Texture(adder + "Hero_slide00.png");
		}
		if (this.stateCheck() == STATE_JUMP)
		{
			return new Texture(adder + "Hero_jump00.png");
		}
		return this.heroSprite;
	}
	
	private void updateState()
	{
		if(this.starter)
		{
			if(this.activeState == STATE_HIT)
			{
				if(this.activeFrame != 0)
				{
					return;
				}
			}
			if(Gdx.input.isKeyPressed(checkInput(isFliped)) && ((this.position.y == 60 && !isFliped )|| (this.position.y == 420-85-60 && isFliped) ))
			{
					this.activeState = STATE_SLIDE;
			}
			else if(Gdx.input.isKeyJustPressed(checkInput(!isFliped)) && ((this.position.y == 60 && !isFliped) || (this.position.y == 420-85-60 && isFliped )))
			{
					this.activeState = STATE_JUMP;
					this.jumpSound.play();
					jumpStart ++;
			}
			else
				this.activeState = STATE_WALK;
			return;
		}
		if (Gdx.input.isKeyPressed(Keys.ANY_KEY))
			this.starter = true;
	}
	private int frameCheck()
	{
		return this.activeFrame;
	}
	private int stateCheck()
	{
		return activeState;
	}
	private int checkInput(boolean isFliped)
	{
		if (isFliped)
			return Keys.UP;
		return Keys.DOWN;
	}
	
	public void damageTaken()
	{
		if(this.activeState != STATE_HIT)
		{
			this.activeState = STATE_HIT;
			this.hitSound.play();
			//this.remainLife -= 1;
			//System.out.println("Hit");
		}
	}
	
	public Vector2 getPosition()
	{
		return this.position;
	}
	public Texture getSprite()
	{
		return this.heroSprite;
	}
	public Rectangle getBound()
	{
		if (!isFliped)
		{
			if(activeState != STATE_SLIDE)
			{
				return new Rectangle((int)getPosition().x+58, (int)getPosition().y+17, 11, 55);
			}
			else
			{
				return new Rectangle((int)getPosition().x+9, (int)getPosition().y+14, 55, 11);
			}
		}
		else
		{
			if (activeState != STATE_SLIDE)
			{
				return new Rectangle((int)getPosition().x+58, (int)getPosition().y+85-17-55, 11, 55);
			}
			else
			{
				return new Rectangle((int)getPosition().x+9, (int)getPosition().y+85-14-11, 55, 11);
			}
		}
	}
}
