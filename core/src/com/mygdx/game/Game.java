package com.mygdx.game;

public class Game 
{
	private Hero hero;
    private RevUP revUP;
    private Map map;
    private int gameSpeed = 1;
    private int score;
    private int life = 3;
    
	Game(RevUP revUP)
	{
        hero = new Hero(60,60);
        this.revUP = revUP;
        this.map = new Map();
        score = 0;
	}
	
	public Hero getHero()
	{
		return this.hero;
	}
	public RevUP getRevUP()
    {
    	return revUP;
    }
	public Map getMap()
	{
		return this.map;
	}
	public void addGameSpeed()
    {
    	gameSpeed += 1;
    	
    }
    public int getGameSpeed()
    {
    	return gameSpeed;
    }
    public int getScore() 
    {
        return score;
    }
    public void increaseScore() 
    {
        score += 5;
    }
    public int getLife() 
    {
        return life;
    }
    public void decreaseLife() 
    {
        life -= 1;
    }
}
