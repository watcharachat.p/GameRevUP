package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;

public class GameScreen extends ScreenAdapter 
{
	private RevUP revUP;
	private GameRenderer gameRenderer;
	private Game game;
	private Hero hero;
	 
    public GameScreen(RevUP revUP) 
    {
        this.revUP = revUP; 
        game = new Game(revUP);
        hero = game.getHero();
        gameRenderer = new GameRenderer(revUP,game);
    }
    
    @Override
    public void render(float delta) 
    {
    	Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    	gameRenderer.render(delta);
    }
}
