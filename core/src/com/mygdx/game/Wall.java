package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

public class Wall 
{
	private Vector2 position;
	
	public Wall(int x,int y)
	{
		position = new Vector2(x,y);
	}
	
	public void wallSet(float x,float y)
	{
		this.position.x = x;
		this.position.y = y;
	}
	public Vector2 getWallPosition()
	{
		return this.position;
	}
}
